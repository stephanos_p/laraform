<?php

namespace App\Forms;

use Laraform\Laraform;

class NCAForm extends Laraform {

    public $component = 'nca-form';

    public $theme = 'nca-theme';

    // Defining form steps
    public $wizard = [

        // 1st step - 'Start'
        'start' => [
            'label' => 'Start',
            'elements' => ['company_information', 'address', 'optional_address', 'commercial_register', 'stock_exchange_information'],
            'labels' => [
                'previous' => '',
                'next' => 'Continue to CEO & Signatories'
            ]
        ],

        // 2nd step - 'CEO & Signatories'
        'ceo_and_signatories' => [
            'label' => 'CEO & Signatories',
            'elements' => ['ceo'],
            'labels' => [
                'previous' => '< Return to Start',
                'next' => 'Continue to Ownership'
            ]
        ],

        // 3rd step - 'Ownership'
        'ownership' => [
            'label' => 'Ownership',
            'elements' => [],
            'labels' => [
                'previous' => '< Return to CEO & Signatories',
                'next' => 'Continue to Intermediary Companies'
            ]
        ],

        // 4th step - 'Intermediary Companies'
        'intermediary_companies' => [
            'label' => 'Intermediary Companies',
            'elements' => [],
            'labels' => [
                'previous' => '< Return to Ownership',
                'next' => 'Continue to General Information'
            ]
        ],

        // 5th step - 'General Information'
        'general_information' => [
            'label' => 'General Information',
            'elements' => [],
            'labels' => [
                'previous' => '< Return to Intermediary Companies',
                'next' => 'Continue to Upload'
            ]
        ],

        // 6th step - 'Upload'
        'upload' => [
            'label' => 'Upload',
            'elements' => [],
            'labels' => [
                'previous' => '< Return to General Information',
                'finish' => 'Complete process'
            ]
        ],
    ];

    public function schema() {

        return [
            //region START

            'company_id' => [
                'type' => 'key'
            ],

            // 'Company information'
            'company_information' => [
                'type' => 'group',
                'label' => 'Your Company',
                'classes' => [
                    'elementContainer' => 'outermost-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label big-label',
                    'fieldContainer' => 'group-field'
                ],
                'schema' => [
                    'company_name' => [
                        'type' => 'text',
                        'placeholder' => 'Company Name',
                        'rules' => 'required',
                    ],
                    'legal_entity' => [
                        'type' => 'select',
                        'placeholder' => 'Legal Entity',
                        'rules' => 'required',
                        'items' => [
                            'gmbh' => 'GmbH',
                            'ag' => 'AG',
                            'gbr' => 'GbR',
                        ],
                    ],
                ]
            ],

            // 'Address'
            'address' => [
                'type' => 'group',
                'label' => 'Address',
                'classes' => [
                    'elementContainer' => 'outermost-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label',
                    'fieldContainer' => 'group-field'
                ],
                'schema' => [
                    'company_street1' => [
                        'type' => 'text',
                        'placeholder' => 'Street and Street Number',
                        'rules' => 'required',
                    ],
                    'company_zip1' => [
                        'type' => 'text',
                        'placeholder' => 'ZIP',
                        'rules' => 'required',
                        'columns' => 4,
                    ],
                    'company_city1' => [
                        'type' => 'text',
                        'placeholder' => 'City',
                        'rules' => 'required',
                        'columns' => 8,
                    ],
                ]
            ],

            // 'Optional address'
            'optional_address' => [
                'type' => 'group',
                'label' => 'Optional Address',
                'classes' => [
                    'elementContainer' => 'outermost-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label',
                    'fieldContainer' => 'group-field'
                ],
                'schema' => [
                    'company_street2' => [
                        'type' => 'text',
                        'placeholder' => 'Street and Street Number',
                        'rules' => '',
                    ],
                    'company_zip2' => [
                        'type' => 'text',
                        'placeholder' => 'ZIP',
                        'rules' => '',
                        'columns' => 3,
                    ],
                    'company_city2' => [
                        'type' => 'text',
                        'placeholder' => 'City',
                        'rules' => '',
                        'columns' => 9,
                    ],
                ]
            ],

            // 'Commercial Register'
            'commercial_register' => [
                'type' => 'group',
                'label' => 'Commercial Register',
                'classes' => [
                    'elementContainer' => 'outermost-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label',
                    'fieldContainer' => 'group-field'
                ],
                'schema' => [
                    'register' => [
                        'type' => 'select',
                        'placeholder' => 'Register',
                        'rules' => 'required',
                        'items' => [
                            'hra' => 'HRA',
                            'hrb' => 'HRB',
                        ],
                        'columns' => 4,
                    ],
                    'register_no' => [
                        'type' => 'text',
                        'placeholder' => 'Register No.',
                        'rules' => 'required',
                        'columns' => 4,
                    ],
                    'register_city' => [
                        'type' => 'text',
                        'placeholder' => 'Register City',
                        'rules' => 'required',
                        'columns' => 4,
                    ],
                    'extract' => [
                        'type' => 'file',
                    ],
                ]
            ],

            // 'Stock Exchange Information'
            'stock_exchange_information' => [
                'type' => 'group',
                'label' => 'Stock Exchange Information',
                'classes' => [
                    'elementContainer' => 'outermost-container final-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label',
                    'fieldContainer' => 'group-field'
                ],
                'before' => 'Is the company listed on a German or EU stock exchange in accordance with Section 2 (11) of the WpHG?',
                'schema' => [
                    'stock_exchange_listed' => [
                        'type' => 'radiogroup',
                        'items' => [
                            'yes' => 'Yes',
                            'no' => 'No',
                        ],
                        'rules' => 'required',
                        'messages' => [
                            'required' => 'One Stock Exchange option must be chosen.'
                        ],
                    ],

                    // Stock Exchange details (if selected)
                    'stock_exchange_details' => [
                        'conditions' => [
                            ['stock_exchange_information.stock_exchange_listed', 'yes']
                        ],
                        'type' => 'group',
                        'class' => 'stock-exchange-details',
                        'schema' => [
                            // Stock Exchange
                            'stock_exchange_name' => [
                                'type' => 'text',
                                'placeholder' => 'Stock Exchange',
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                            ],
                            // ISIN
                            'stock_exchange_isin' => [
                                'type' => 'text',
                                'placeholder' => 'ISIN',
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                            ],
                            // Issues bearer shares
                            'issues_bearer_shares' => [
                                'type' => 'radiogroup',
                                'before' => 'Does the company issue bearer shares?',
                                'items' => [
                                    'yes' => 'Yes',
                                    'no' => 'No',
                                ],
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                                'messages' => [
                                    'required' => 'This field is required.'
                                ],
                            ],
                            // Bearer shares listed
                            'bearer_shares_listed' => [
                                'type' => 'radiogroup',
                                'before' => 'The bearer shares are listed for trading on a German / EU stock exchange or deposited with an (international) central securities depositary (e.g. Clearstream) or another custodian.',
                                'items' => [
                                    'yes' => 'Yes',
                                    'no' => 'No',
                                ],
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                                'messages' => [
                                    'required' => 'This field is required.'
                                ],
                            ],
                            // Bearer shares evidence
                            'bearer_shares_evidence' => [
                                'type' => 'radiogroup',
                                'before' => "Evidence of the presence of bearer shares and the percentage of the company's capital which are certified as bearer shares is available.",
                                'items' => [
                                    'yes' => 'Yes',
                                    'no' => 'No',
                                ],
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                                'messages' => [
                                    'required' => 'This field is required.'
                                ],
                            ],
                            // Bearer shares more than 10%
                            'bearer_shares_more_than_10_percent' => [
                                'type' => 'radiogroup',
                                'before' => 'Bearer shares make up more than 10% of the capital and are not listed on an exchange or managed by a custodian like the aforementioned bearer shares.',
                                'items' => [
                                    'yes' => 'Yes',
                                    'no' => 'No',
                                ],
                                'rules' => [
                                    [
                                        'required' => ['stock_exchange_listed', 'yes']
                                    ]
                                ],
                                'messages' => [
                                    'required' => 'This field is required.'
                                ],
                            ],
                        ]
                    ],
                ]
            ],

            //endregion

            //region CEO & SIGNATORIES

            // 'CEO'
            'ceo' => [
                'type' => 'group',
                'label' => 'CEO',
                'classes' => [
                    'elementContainer' => 'outermost-container',
                    'elementInner' => 'group-element',
                    'labelContainer' => 'group-label big-label',
                    'fieldContainer' => 'group-field'
                ],
                'schema' => [
                    'ceos' => [
                        'type' => 'list',
                        'object' => [
                            'label' => 'CEO',
                            'classes' => [
                                'labelContainer' => 'group-label',
                            ],
                            'schema' => [
                                'ceo_id' => [
                                    'type' => 'key'
                                ],
                                'ceo_first_name' => [
                                    'type' => 'text',
                                    'placeholder' => 'First Name',
                                    'rules' => 'required',
                                    'columns' => 6
                                ],
                                'ceo_last_name' => [
                                    'type' => 'text',
                                    'placeholder' => 'Last Name',
                                    'rules' => 'required',
                                    'columns' => 6
                                ],
                                'ceo_date_of_birth' => [
                                    'type' => 'date',
                                    'placeholder' => 'Date of Birth',
                                    'rules' => 'required',
                                ],
                                'ceo_street' => [
                                    'type' => 'text',
                                    'placeholder' => 'Street and Street Number',
                                    'rules' => 'required',
                                ],
                                'ceo_zip' => [
                                    'type' => 'text',
                                    'placeholder' => 'ZIP',
                                    'rules' => 'required',
                                    'columns' => 4
                                ],
                                'ceo_city' => [
                                    'type' => 'text',
                                    'placeholder' => 'City',
                                    'rules' => 'required',
                                    'columns' => 8
                                ],
                                'ceo_country' => [
                                    'type' => 'text',
                                    'placeholder' => 'Country',
                                    'rules' => 'required',
                                ],
                                'ceo_role' => [
                                    'type' => 'text',
                                    'placeholder' => 'Role',
                                    'rules' => 'required',
                                ],
                                'fictitious_beneficial_owner' => [
                                    'type' => 'radiogroup',
                                    'before' => "Is the company's owner a fictitious beneficial owner?",
                                    'items' => [
                                        'yes' => 'Yes',
                                        'no' => 'No',
                                    ],
                                    'rules' => 'required',
                                    'messages' => [
                                        'required' => 'One option must be chosen.'
                                    ],
                                ],
                                'nationality' => [
                                    'type' => 'text',
                                    'placeholder' => 'Nationality',
                                    'rules' => 'required',
                                ],
                                'source' => [
                                    'type' => 'text',
                                    'placeholder' => 'Source',
                                    'rules' => 'required',
                                ],
                                'documentation_attached' => [
                                    'type' => 'checkbox',
                                    'text' => 'Documentation is attached',
                                ],
                            ]
                        ]
                    ]
                ]
            ],

            //endregion

            //region OWNERSHIP


            //endregion

            //region INTERMEDIARY COMPANIES


            //endregion

            //region GENERAL INFORMATION


            //endregion

            //region UPLOAD


            //endregion
        ];
    }

    public function after() {
        //region START

        $company = \App\Models\Company::findOrNew($this->data['company_id']);

        $company->name = $this->data['company_name'];
        $company->legal_entity = $this->data['legal_entity'];
        $company->street1 = $this->data['company_street1'];
        $company->zip1 = $this->data['company_zip1'];
        $company->city1 = $this->data['company_city1'];
        $company->street2 = $this->data['company_street2'];
        $company->zip2 = $this->data['company_zip2'];
        $company->city2 = $this->data['company_city2'];
        $company->register = $this->data['register'];
        $company->register_no = $this->data['register_no'];
        $company->register_city = $this->data['register_city'];
        if ($this->data['stock_exchange_listed'] === 'yes') {
            $company->stock_exchange_listed = 1;
            $company->stock_exchange_name = $this->data['stock_exchange_name'] ?? null;
            $company->stock_exchange_isin = $this->data['stock_exchange_isin'] ?? null;
            $company->issues_bearer_shares = $this->data['issues_bearer_shares'] === 'yes' ? 1 : 0;
            $company->bearer_shares_listed = $this->data['bearer_shares_listed'] === 'yes' ? 1 : 0;
            $company->bearer_shares_evidence = $this->data['bearer_shares_evidence'] === 'yes' ? 1 : 0;
            $company->bearer_shares_more_than_10_percent = $this->data['bearer_shares_more_than_10_percent'] === 'yes' ? 1 : 0;
        } else {
            $company->stock_exchange_listed = 0;
        }

        $company->save();

        //endregion

        //region CEO & SIGNATORIES

        foreach ($this->data['ceos'] as $form_ceo) {
            $ceo = \App\Models\Ceo::findOrNew($form_ceo['ceo_id']);

            $ceo->company_id = $company->id;
            $ceo->first_name = $form_ceo['ceo_first_name'];
            $ceo->last_name = $form_ceo['ceo_last_name'];
            $ceo->date_of_birth = $form_ceo['ceo_date_of_birth'];
            $ceo->street = $form_ceo['ceo_street'];
            $ceo->zip = $form_ceo['ceo_zip'];
            $ceo->city = $form_ceo['ceo_city'];
            $ceo->country = $form_ceo['ceo_country'];
            $ceo->role = $form_ceo['ceo_role'];
            $ceo->fictitious_beneficial_owner = $form_ceo['fictitious_beneficial_owner'] === 'yes' ? 1 : 0;
            $ceo->nationality = $form_ceo['nationality'];
            $ceo->source = $form_ceo['source'];
            $ceo->documentation_attached = $form_ceo['documentation_attached'] === 'true' ? 1 : 0;

            $ceo->save();
        }

        //endregion
    }
}
