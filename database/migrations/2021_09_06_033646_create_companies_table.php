<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('legal_entity');
            $table->string('street1');
            $table->string('zip1');
            $table->string('city1');
            $table->string('street2')->nullable();
            $table->string('zip2')->nullable();
            $table->string('city2')->nullable();
            $table->string('register');
            $table->string('register_no');
            $table->string('register_city');
            $table->boolean('stock_exchange_listed');
            $table->string('stock_exchange_name')->nullable();
            $table->string('stock_exchange_isin')->nullable();
            $table->boolean('issues_bearer_shares')->nullable();
            $table->boolean('bearer_shares_listed')->nullable();
            $table->boolean('bearer_shares_evidence')->nullable();
            $table->boolean('bearer_shares_more_than_10_percent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
