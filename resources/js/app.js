require('./bootstrap');

import Vue from 'vue/dist/vue.js'
import Laraform from '@laraform/laraform/src'
import NCATheme from './themes/nca_theme'
import NCAForm from './components/forms/NCAForm.vue'

Laraform.theme('nca-theme', NCATheme)

Vue.use(Laraform)

Vue.component('nca-form', NCAForm)

const app = new Vue({
    el: '#app',
})
