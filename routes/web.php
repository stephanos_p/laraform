<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/nca', function () {
    return view('nca', [
        'form' => app('App\Forms\NCAForm')
    ]);
});

Route::match(['get', 'post'],'/nca/{id}', function ($id) {
    $company = \App\Models\Company::find($id);

    //region START

    $data = [
        'company_id' => $company->id,
        'company_name' => $company->name,
        'legal_entity' => $company->legal_entity,
        'company_street1' => $company->street1,
        'company_zip1' => $company->zip1,
        'company_city1' => $company->city1,
        'company_street2' => $company->street2,
        'company_zip2' => $company->zip2,
        'company_city2' => $company->city2,
        'register' => $company->register,
        'register_no' => $company->register_no,
        'register_city' => $company->register_city,
        'stock_exchange_listed' => $company->stock_exchange_listed ? 'yes' : 'no',
        'stock_exchange_name' => $company->stock_exchange_name,
        'stock_exchange_isin' => $company->stock_exchange_isin,
        'issues_bearer_shares' => !is_null($company->issues_bearer_shares) ? ($company->issues_bearer_shares ? 'yes' : 'no') : null,
        'bearer_shares_listed' => !is_null($company->bearer_shares_listed) ? ($company->bearer_shares_listed ? 'yes' : 'no') : null,
        'bearer_shares_evidence' => !is_null($company->bearer_shares_evidence) ? ($company->bearer_shares_evidence ? 'yes' : 'no') : null,
        'bearer_shares_more_than_10_percent' => !is_null($company->bearer_shares_more_than_10_percent) ? ($company->bearer_shares_more_than_10_percent ? 'yes' : 'no') : null,
    ];

    //endregion

    //region CEO & SIGNATORIES

    $ceos = $company->ceos;

    foreach ($ceos as $ceo) {
        $data['ceos'][] = [
            'ceo_id' => $ceo->id,
            'ceo_first_name' => $ceo->first_name,
            'ceo_last_name' => $ceo->last_name,
            'ceo_date_of_birth' => $ceo->date_of_birth,
            'ceo_street' => $ceo->street,
            'ceo_zip' => $ceo->zip,
            'ceo_city' => $ceo->city,
            'ceo_country' => $ceo->country,
            'ceo_role' => $ceo->role,
            'fictitious_beneficial_owner' => $ceo->fictitious_beneficial_owner ? 'yes' : 'no',
            'nationality' => $ceo->nationality,
            'source' => $ceo->source,
            'documentation_attached' => $ceo->documentation_attached ? 'true' : 'false',
        ];
    }

    //endregion

    return view('nca', [
        'form' => app('App\Forms\NCAForm')->load($data)
    ]);
});
